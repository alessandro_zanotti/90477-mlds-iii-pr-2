# Tsunami Dataset 

This dataset, also known as The Global Historical Tsunami Database, represents historical tsunamis and related information 
provides information on over 2,400 tsunamis from 2100 BC to the present in the Atlantic, Indian, and Pacific Oceans; and the 
Mediterranean and Caribbean Seas.

The dataset contains Tsunami events records from 2100 BC to 2020, 2259 observation on 21 variables.

- ID identifier of the event
- YEAR self explained 
- MONTH self explained 
- DAY self explained 
- ....
- ....
- LATITUDE
- LONGITUDE
- LOCATION_NAME
- COUNTRY
- CAUSE: cause of the tsunami 
- EVENT_VALIDITY: Trustworthiness of the record
- EQ_MAGNITUDE: Earthquake (that caused the tsunami) magnitude
- EQ_DEPTH: Earthquake depth
- DAMAGE_TOTAL_DESCRIPTION: Total damage, estimed in US dollars
- HOUSE_TOTAL_DESCRIPTION: Total amount of houses destroyed
- DEATH_TOTAL_DESCRIPTION: Total amount of deaths
- ....
